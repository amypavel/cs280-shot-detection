
# In[2]:

import cv2
import numpy as np
from sklearn import svm
from sklearn import preprocessing
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report
import sklearn
import os

def build_svm(features, labels): 
    standard_scaler = preprocessing.StandardScaler()
    X = np.array(features)
    scale_X = standard_scaler.fit_transform(X)
    y = np.array(labels)
    clf = svm.SVC(kernel='linear')
    clf = clf.fit(scale_X, y)
    return (clf, standard_scaler)

def build_random_forest(features, labels):
    X = np.array(features)
    y = np.array(labels)
    clf = RandomForestClassifier(n_estimators=300)
    clf = clf.fit(X, y)
    return clf

def build_logistic_regression(features, labels): 
    X = np.array(features)
    y = np.array(labels)
    clf = sklearn.linear_model.LogisticRegression(class_weight="auto")
    clf = clf.fit(X,y)
    return clf

def predict(clf, feature_vector, min_max_scaler=None): 
    if min_max_scaler: 
        # its an SVM!
        hist = min_max_scaler.transform([feature_vector])
        return clf.predict(feature_vector)
    else: 
        # its a random forest!
        return clf.predict([feature_vector])


# In[5]:

ROOT = '/Users/apavel/Desktop/peak_finding/'
CANDIDATES = ['anni005', 'anni009', 'bor03', 'bor17', 'nad28', 
              'nad31', 'nad33', 'nad53', 'nad57', 'senses111']
N = 5000


# In[20]:

import json

# get all the feature jsons
# jsons = [j for j in os.listdir(root) if '.json' in j]
jsons = ['color_results.json', 'lum_results.json', 'plain_mag_results.json']

truth_dict = {}
feature_dict = {}
# initialize feature dict
for candidate in CANDIDATES: 
    feature_dict[candidate] = [[]]*N
    truth_dict[candidate] = [[]]*N
feature_list = []

for fn in jsons: 
    print fn
    with open(root + fn) as j: 
        feature_list.append(fn.split('_')[0])
        feature_json = json.load(j)
        for candidate in CANDIDATES: 
            # get the actual and detected info
            rel_dict = [f for f in feature_json if f['candidate'] == candidate][0]
            actual = [f for f in rel_dict["actual"] if f < N]
            detected = [f for f in rel_dict["detected"] if f < N]
            
            # make feature vectors and truth label vectors
            for i in range(0,N): 
                print i, truth_dict
                if i in actual: 
                    truth_dict[candidate][i].append(1)
                else:
                    truth_dict[candidate][i].append(0)
                if i in detected: 
                    feature_dict[candidate][i].append(1)
                else: 
                    feature_dict[candidate][i].append(0)
            


# In[ ]:

jsons = [j for j in os.listdir(root) if '.json' in j]
print jsons


# In[19]:

[]*50


# In[ ]:



