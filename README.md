# README #

### Run peak detection and classification ###

* Compute features and detect peaks: Open shotsshotsshotsshots..., change configuration variables, and run the first 3 cells
* Run classification: Open peak_based_classifier, change configuration variables, and run first 3 cells
* If you would like to try out peak_based_classifier without first running the peak detection code, you can use the pre-computed features in "peak_finding/"
* Let me know your questions!